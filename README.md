# LXD server

Installation and configuration of the LXD service, 
the default client utility `lxc`, and a default LXD profile.

Note that installation of individual LXD containers should be handled
in their respective roles, for example Wallabag.

This role does not presently manage CPU resources.
Perhaps you would want to look into that. How does LXD distribute CPU in the absence of user-choice?
It seems all CPU cores are simply available to the container(s). Also, it appears all swap 
is similarly available to the container(s).
https://www.jamescoyle.net/how-to/2532-setting-cpu-resource-limits-with-lxc
https://lxd.readthedocs.io/en/latest/instances/


> Note: LXD comes pre-installed in Ubuntu 18.04, and the installed LXD package is a deb package.
> But beginning with Ubuntu 20.04, newer versions of LXD are now only available as snap packages.
> https://www.digitalocean.com/community/tutorials/how-to-set-up-and-use-lxd-on-ubuntu-18-04

LXD is composed of

+ the LXD service
+ the default client utility, lxc

The client utility can only access the LXD service if you run it as root, or
if your non-root account is a member of the `lxd` unix group.

> By default LXD server is not accessible from the network as it only listens on a local unix socket.
> By default, all Ubuntu LXD images for containers are set up with `PasswordAuthentication no` in their SSH configuration.

+ https://linuxcontainers.org/lxd/docs/stable-4.0/
+ https://askubuntu.com/questions/1106369/how-to-ssh-into-a-lxd-guest

This would suit me fine, as I intend to run Apache as TLS-terminating reverse proxy on the host.

SSH into LXD container should work as for any other host.
We can even pre-populate our pubkey into all LXD container by adding it to our LXD profile.
https://gist.github.com/jeanlouisferey/15be1f421eb9f9a66f1c74d410de2675


## LXD init

How to handle `lxd init`?
For bionic, it is the first step (before this role).
But for focal, it would be the second step, after `snap install lxd`.
https://discuss.linuxcontainers.org/t/how-to-get-the-full-configuration-to-use-it-with-lxd-init-preseed/3333/4

According to [ahnooie](https://github.com/ahnooie/ansible-role-lxd-container)
the [Ansible role by juju](https://github.com/juju4/ansible-lxd) can perform 
the equivalent of `lxd init`.

In order to learn more, I executed `lxd init` interactively:

```
taha@luxor:~
$ lxd init
Would you like to use LXD clustering? (yes/no) [default=no]: 
Do you want to configure a new storage pool? (yes/no) [default=yes]: no
Would you like to connect to a MAAS server? (yes/no) [default=no]: 
Would you like to create a new local network bridge? (yes/no) [default=yes]: 
What should the new bridge be called? [default=lxdbr0]: 
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 
Would you like LXD to be available over the network? (yes/no) [default=no]: 
Would you like stale cached images to be updated automatically? (yes/no) [default=yes] 
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]: yes
config: {}
networks:
- config:
    ipv4.address: auto
    ipv6.address: auto
  description: ""
  managed: false
  name: lxdbr0
  type: ""
storage_pools: []
profiles:
- config: {}
  description: ""
  devices:
    eth0:
      name: eth0
      nictype: bridged
      parent: lxdbr0
      type: nic
  name: default
cluster: null
```

This `lxd init` procedure led to the creation of several files (except for `unix.socket` which preceded this command):

```
root@luxor:~
# tree /var/lib/lxd/
/var/lib/lxd/
├── containers
├── database
│   ├── global
│   │   ├── logs.db
│   │   └── snapshots
│   └── local.db
├── devices
├── devlxd
│   └── sock
├── disks
├── images
├── networks
│   └── lxdbr0
│       ├── dnsmasq.hosts
│       ├── dnsmasq.leases
│       ├── dnsmasq.pid
│       └── dnsmasq.raw
├── security
├── server.crt
├── server.key
├── shmounts
├── snapshots
├── storage-pools
└── unix.socket

15 directories, 9 files
```

It also created a network device:

```
root@luxor:~
# ip a
14: lxdbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 1e:c7:fa:97:9f:bd brd ff:ff:ff:ff:ff:ff
    inet 10.92.140.1/24 scope global lxdbr0
       valid_lft forever preferred_lft forever
    inet6 fd42:dc7e:8eb9:916::1/64 scope global 
       valid_lft forever preferred_lft forever
    inet6 fe80::1cc7:faff:fe97:9fbd/64 scope link 
       valid_lft forever preferred_lft forever
```

For reference, this is the default LXD profile:

```
root@luxor:~
# lxc profile show default
config: {}
description: Default LXD profile
devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: lxdbr0
    type: nic
name: default
used_by: []
```


## LXD storage

When you configure LXD to use the `dir` storage backend,
containers are located at `/var/lib/lxd/containers/` by default.

+ https://blog.simos.info/how-to-initialize-lxd-again/

> For the `dir` backend, there is no disk size restriction to speak of; 
> the available space is whatever disk space you have available on the parent device, 
> typically your system's root device.
> https://discuss.linuxcontainers.org/t/how-to-check-lxd-container-size-and-how-much-space-they-are-tacking/4770/7

This means that `df` inside any container, or on the parent host, or `lxc storage info default`
will all match as far as used and free space is concered (keep in min unit conversion GB/GiB).


## User mapping between LXD container and host

This is how it looks before any changes inside an LXD container:
```
taha@hunan:~
$ cat /etc/subuid
lxd:100000:65536
root:100000:65536
taha:165536:65536
$ cat /etc/subgid
lxd:100000:65536
root:100000:65536
taha:165536:65536
```

And likewise on the host:
```
taha@luxor:~
$ cat /etc/subuid
lxd:100000:65536
root:100000:65536
taha:165536:65536
$ cat /etc/subgid
lxd:100000:65536
root:100000:65536
taha:165536:65536
```

> The fact that all uids/gids in an unprivileged container are mapped 
> to a normally unused range on the host means that sharing of data 
> between host and container is effectively impossible.
> https://stgraber.org/2017/06/15/custom-user-mappings-in-lxd-containers/

`raw.idmap` should be set to `both 1000 1000`.
(`both` denotes that both uid and gid should be set, the second entry on the line
is the source id, i.e., the id on the host, and the third entry
is the id inside the container).

This property requires a container reboot to take effect.

Alot of incomplete guides (and some with outright dangerous advice) on how to mount a directory
on the host inside your LXD container (see links below).

+ https://linuxcontainers.org/lxd/docs/master/userns-idmap
+ https://lxd.readthedocs.io/en/latest/userns-idmap/
+ https://superuser.com/questions/1174344/syntax-for-setting-lxd-container-raw-idmap/
+ https://github.com/lxc/lxd/issues/579
+ https://github.com/lxc/lxd/issues/1879
+ https://gist.github.com/bloodearnest/ebf044476e70c4baee59c5000a10f4c8
+ https://ubuntu.com/blog/mounting-your-home-directory-in-lxd


## Mount directory from host on the LXD container

Without user mapping, such mounts are effectively read-only (mounted as `nobody:nogroup`).

> NOTE: it's **not possible** to mount NFS from inside unprivileged LXD containers.
> Trying gives: `Error mounting /media/bay: mount.nfs: Operation not permitted`.

+ https://reddit.com/r/LXD/comments/9hd9r6/mount_a_host_dir_inside_an_container/
+ https://www.cyberciti.biz/faq/how-to-add-or-mount-directory-in-lxd-linux-container/
+ https://linuxcontainers.org/lxd/docs/master/instances#type-disk
+ https://github.com/lxc/lxd/issues/1047
+ https://theorangeone.net/posts/mount-nfs-inside-lxc/
+ https://discuss.linuxcontainers.org/t/nfs-directly-inside-container/9841
+ https://gist.github.com/julianlam/07abef272136ea14a627
+ https://stgraber.org/2017/06/15/custom-user-mappings-in-lxd-containers/


## LXD and KVM on the same host?

I had the right hunch, this should not present a problem.
https://opennebula.io/using-lxd-and-kvm-on-the-same-host/


## A note on migrating LXD between hosts

The following verbatim from [Simon Xenitellis' answer](https://blog.simos.info/how-to-initialize-lxd-again/).

The full state of a LXD installation is stored in `/var/lib/lxd`.
~~Specifically, `/var/lib/lxd/lxd.db` is an SQLite database with information about each container 
along with the information about the storage pool (ZFS).~~
This database file does not exist on my system. 

The easy way would be to
1. stop the LXD service on A
2. stop the (empty) LXD server on B
3. copy over `/var/lib/lxd/` from A to the same location on B.
4. start the LXD service on B.

If you need to make individual changes, you may edit `/var/lib/lxd/lxd.db` using the program "sqlitebrowser".


## MAC address assignment of LXD containers

This only matters for the DNS server, which at presently uses the `br0` network parent
and thus receives an IP address by my router's DHCP server.

```
taha@luxor:~
$ lxc list 
+--------+---------+----------------------+-----------------------------------------------+------------+-----------+
|  NAME  |  STATE  |         IPV4         |                     IPV6                      |    TYPE    | SNAPSHOTS |
+--------+---------+----------------------+-----------------------------------------------+------------+-----------+
| chia   | RUNNING | 192.168.1.137 (eth0) |                                               | PERSISTENT | 0         |
+--------+---------+----------------------+-----------------------------------------------+------------+-----------+
taha@luxor:~
$ lxc config show chia 
architecture: x86_64
config:
  image.description: ubuntu 20.04 LTS amd64 (release) (20210622)
  volatile.eth0.hwaddr: 00:16:3e:bf:79:4e
  volatile.eth0.name: eth0
devices:
  eth0:
    ipv4.address: ""
    nictype: bridged
    parent: br0
    type: nic
profiles:
- dns-server
```

The router lists the following IP address assignment:
```
192.168.1.137   00:16:3e:bf:79:4e   2021/06/27  13:52:57  fsdhcp  chia
```

So the first question is, what happens to the container's MAC address when container restarts?
Ok, MAC does not change by simply restarting the container.
```
lxc restart chia
lxc list
lxc config show chia
```

What about deleting the container and recreating it?
The newly created container gets a new MAC address and thus a new IP address assignment from the DHCP pool by the router.

To be able to guarantee that the DNS server is reachable on a static IP address, it appears we need
to actively set the MAC address of the container, so it doesn't get reset if the container is destroyed/recreated.

I have enabled `hwaddr` in the `devices: eth0:` section of the container settings, and after setting it,
note how the new MAC address is no longer specified as `volatile.eth0.hwaddr`:
```
taha@luxor:~
$ lxc config show chia 
architecture: x86_64
config:
  image.description: ubuntu 20.04 LTS amd64 (release) (20210622)
  volatile.eth0.name: eth0
devices:
  eth0:
    hwaddr: 00:16:3e:bf:79:4e
    ipv4.address: ""
    nictype: bridged
    parent: br0
    type: nic
profiles:
- dns-server
```

I can confirm that this manually set MAC address is in use by the container, and seen by the router.
Now I have set this MAC address as a static IP mapping in the router's DCHP settings.
Let's now confirm that the container remains reachable on the same IP address after destroying/recreating it again.

Yes, that works as expected!

> Sidenote: [explanation of the @-notation](https://unix.stackexchange.com/questions/358982/network-interface-name-has-with-at-sign-what-is-it) used in network interface names on the container.


## Fan networking

This was a new subject for me.
Let's not bother with fan networking.

> Fan trades access to one user-selected `/8` (potentially external) address range for an expanded pool of "organisation-internal" addresses to be used by containers or virtual machines. 
> Fan does this by mapping the addresses in a way that can be computed, rather than one that requires maintenance of distributed state (e.g., routing tables).

+ https://wiki.ubuntu.com/FanNetworking (a very informative article on the subject)
+ https://juju.is/docs/olm/fan-container-networking
+ https://blog.simos.info/how-to-make-your-lxd-containers-get-ip-addresses-from-your-lan-using-a-bridge/
+ http://manpages.ubuntu.com/manpages/bionic/man8/fanatic.8.html



## Stopping and deleting LXD containers

First stop, then delete:
```
lxc stop <hostname>
lxc delete <hostname>
```

This also deletes all files in the container's storage pool.



## Autostarting (unprivileged) LXD containers

```
$ lxc config set {vm-name} boot.autostart {true|false}
$ lxc config set {vm-name} boot.autostart.priority integer
$ lxc config set {vm-name} boot.autostart.delay integer
```

For `boot.autostart.priority`, higher number means higher priority.
Delay `boot.autostart.delay` in seconds to wait after a container starts before starting the next one (default zero).

+ https://linuxcontainers.org/lxc/manpages/man1/lxc-autostart.1.html#lbAF
+ https://www.cyberciti.biz/faq/how-to-auto-start-lxd-containers-at-boot-time-in-linux/
+ https://bitsandslices.wordpress.com/2015/08/26/autostarting-lxd-containers/
+ https://serverfault.com/questions/620709/how-to-auto-start-unprivileged-lxc-containers


## Giving multiple profiles to a container or have profiles inherit from 'default'

None of which has worked for me, so far.
After wiping the profiles on the LXD server, and re-running this role
it seems apparent that the other profiles *do not* build on the default.

I have found multiple sources stating/showing how LXC commands can be used to create containers
using multiple profiles (if any keys overlap, the last one specified overrides).

The way the `lxd_container` module is written suggests that we should be able to pass it multiple profiles.
But all my attempts have so far failed with `Requested profile '['default', 'rstudio-server']' doesn't exist`.

I'm using the latest version of the `community.general` collection:
```
taha@asks2:~
$ ansible-galaxy collection list community.general
# /home/taha/.local/lib/python3.6/site-packages/ansible_collections
Collection        Version
----------------- -------
community.general 3.2.0  
```

A new key (not yet in the docs, as far as I can see) was 
[introduced recently](https://github.com/ansible-collections/community.general/pull/1813)
called `merge_profile`.

No, same error. Even after setting `merge_profile: true` in all my profiles, giving more than 
one profile in a container still gives the same error.

I must concede that I do not understand why profile inheritance/merge does not work for me.
Since it should work for LXC itself, I assume that the Ansible modules support it.
Perhaps it's caused by something I did during `lxc init` in this role?


+ https://docs.ansible.com/ansible/latest/user_guide/collections_using.html#listing-collections
+ https://github.com/ansible-collections/community.general/pull/1813/commits/93b99bd3b550fac88d059f4f9325d7db7e2c8a4a
+ https://reddit.com/r/ansible/comments/c5nak4/ansible_281_and_lxd_profile_module_how_to_inherit/
+ https://github.com/ansible-collections/community.general/pull/2026



## Backup and restore container snapshots

```
$ lxc snapshot <container-name> <snapshot-label>
$ lxc restore <container-name> <snapshot-label>
```

Note that snapshots are saved in `/var/lib/lxd/snapshots/`.

To delete a snapshot:
```
$ lxc info <container-name>
$ lxc delete <container-name>/<snapshot-name>

```

+ https://www.cyberfella.co.uk/2018/08/07/linux-containers-with-lxc-lxd/
+ https://serverok.in/lxc-snapshot
+ https://discuss.linuxcontainers.org/t/managing-lxd-container-snapshots/1951


## Refs

+ https://docs.ansible.com/ansible/latest/collections/community/general/lxd_profile_module.html
+ https://docs.ansible.com/ansible/latest/collections/community/general/lxd_container_module.html
+ https://github.com/juju4/ansible-lxd
+ https://github.com/hispanico/ansible-lxd
+ https://github.com/ahnooie/ansible-role-lxd-container
+ https://github.com/ojan/ansible-role-lxc
+ https://antonneld.wordpress.com/2019/01/02/setting-up-my-lxd-containers-with-ansible-playbooks
+ https://linuxcontainers.org/lxd/getting-started-cli
+ https://uk.images.linuxcontainers.org
+ https://stgraber.org/2016/03/19/lxd-2-0-your-first-lxd-container-312/
+ https://lxd.readthedocs.io/en/stable-3.0/containers/
+ http://www.radicalmatt.com/blog/testing-ansible-in-lxc-containers/
+ https://ubuntu.com/blog/lxd-5-easy-pieces
